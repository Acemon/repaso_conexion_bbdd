import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * Created by Fernando on 13/12/2016.
 */
public class Controller implements ActionListener {
    Ventana view;
    Connection conexion;
    public Controller(Ventana view) {
        this.view = view;
        view.btAnadir.addActionListener(this);
        view.btModificar.addActionListener(this);
        view.btEliminar.addActionListener(this);
        view.btFuncion.addActionListener(this);
        view.btProcedimiento.addActionListener(this);
        try {
            ConectarBBDD();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void ConectarBBDD() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/prueba", "root", "");
    }

    @Override
    public void actionPerformed(ActionEvent a) {
        if (a.getActionCommand().equalsIgnoreCase("Anadir")){
            try {
                anadir();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if (a.getActionCommand().equalsIgnoreCase("Modificar")){
            try {
                modificar();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if (a.getActionCommand().equalsIgnoreCase("Eliminar")){
            try {
                eliminar();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }else if (a.getActionCommand().equalsIgnoreCase("Procedimiento")){
            try {
                procedimiento();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "no existen Procedimientos", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            }
        } else if (a.getActionCommand().equalsIgnoreCase("Funcion")){
            try {
                funcion();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "no existen Funciones", "Aviso", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    private void funcion() throws SQLException {
        String sentenciaSQL = "SELECT llamar_funcion";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        ResultSet rs = sentencia.executeQuery();
        rs.next();
        if (rs.wasNull()){
            JOptionPane.showMessageDialog(null, "Esta Vacio", "Aviso", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, rs.getString(1), "Aviso", JOptionPane.OK_OPTION);
        }
        sentencia.close();
        rs.close();
    }

    private void procedimiento() throws SQLException {
        String sentenciaSQL = "call Eliminar_uno(?)";
        CallableStatement sentencia = conexion.prepareCall(sentenciaSQL);
        sentencia.setString(1, view.textField1.getText());
        sentencia.execute();
        sentencia.close();
    }

    private void eliminar() throws SQLException {
        String sentenciaSQL = "Delete From uno where campouno = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, view.textField1.getText());
        sentencia.executeUpdate();
        if (sentencia != null)
            sentencia.close();
    }

    private void modificar() throws SQLException {
        String sentenciaSQL = "Update uno set campouno = ? WHERE campouno = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(2, view.textoMod.getText());
        sentencia.setString(1, view.textField1.getText());
        sentencia.executeUpdate();
        if (sentencia!= null)
            sentencia.close();
    }

    private void anadir() throws SQLException {
        String sentenciaSql = "INSERT INTO uno (campouno) VALUES (?)";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, view.textField1.getText());
        sentencia.executeUpdate();
        if (sentencia!=null)
            sentencia.close();
    }
}
